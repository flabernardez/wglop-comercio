<?php

get_header(); ?>

	<main id="content" class="site-content">
        <nav id="front-navigation" class="category-navigation">
            <section class="wrapper">
                <ul class="category-list">

		            <?php

		            $args = array(
			            'taxonomy'     => 'product_cat',
			            'orderby'      => 'count',
			            'title_li'     => '',
			            'hide_empty'   => 0,
                        'number'       => 4,
		            );
		            $all_categories = get_categories( $args );
		            foreach ($all_categories as $cat) {
			            if($cat->category_parent == 0 ) {
				            echo '<li><a href="#'. $cat->slug .'">'. $cat->name .'</a></li>';
			            }
		            }
		            ?>
                    <li id="category-list-toggle">
                        <?php _e( 'More ⌄', 'wglop-comercio-theme' )?>
                        <ul id="category-list-rest">

		                    <?php

		                    $args = array(
			                    'taxonomy'     => 'product_cat',
			                    'orderby'      => 'count',
			                    'title_li'     => '',
			                    'hide_empty'   => 0,
			                    'offset'       => 4,
			                    'number'       => 9999
		                    );
		                    $all_categories = get_categories( $args );
		                    foreach ($all_categories as $cat) {
			                    if($cat->category_parent == 0 ) {
				                    echo '<li><a href="#'. $cat->slug .'">'. $cat->name .'</a></li>';
			                    }
		                    }
		                    ?>
                        </ul>
                    </li>
                </ul>
            </section>
		</nav>

        <?php
        if ( have_posts() ) :

            /* Start the Loop */
            while ( have_posts() ) : the_post(); ?>

                <article id="front-page-post" <?php post_class(); ?>>
                    <section class="wrapper">
                        <section class="entry-content">

		                    <?php

		                    the_content( sprintf(
			                    wp_kses(
			                    /* translators: %s: Name of current post. Only visible to screen readers */
				                    __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'wglop-comercio-theme' ),
				                    array(
					                    'span' => array(
						                    'class' => array(),
					                    ),
				                    )
			                    ),
			                    get_the_title()
		                    ) );
		                    ?>

                        </section><!-- .entry-content -->
                    </section>
		        </article><!-- #post-<?php the_ID(); ?> -->

        <?php
        endwhile;

        endif;

        ?>

	</main><!-- #content -->

<?php

get_footer(); ?>