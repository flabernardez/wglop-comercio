<?php

if ( ! function_exists( 'wglop_comercio_theme_setup' ) ) :

	function wglop_comercio_theme_setup() {

		load_theme_textdomain( 'wglop-comercio-theme', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		add_post_type_support( 'page', 'excerpt' );

		add_filter('jpeg_quality', function($arg){return 100;});

		register_nav_menus( array(
			'menu-1' => esc_html__( 'Header', 'wglop-comercio-theme' ),
		) );

		register_nav_menus( array(
			'menu-2' => esc_html__( 'Footer', 'wglop-comercio-theme' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-logo', array(
			'height'      => 60,
			'width'       => 180,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'wglop_comercio_theme_setup' );

function wglop_comercio_theme_customize_register( $wp_customize ) {

	// Accent color
	$wp_customize->add_setting( 'accent_color', array(
		'default'   => '#7DA81C',
		'transport' => 'refresh',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'accent_color', array(
		'section' => 'colors',
		'label'   => esc_html__( 'Accent color', 'theme' ),
	) ) );

}
add_action( 'customize_register', 'wglop_comercio_theme_customize_register' );

function wglop_comercio_theme_get_customizer_css() {

	$accent_color = get_theme_mod( 'accent_color', '#7DA81C' );?>

    <style type="text/css">

		button,
		.button,
		input[type="button"],
		input[type="reset"],
		input[type="submit"],
		.entry-content .wp-block-button__link,
		.wp-block-button__link,
		.woocommerce #respond input#submit.alt,
		.woocommerce a.button.alt,
		.woocommerce button.button.alt,
		.woocommerce input.button.alt,
		.main-navigation.toggled,
		.wp-block-button__link,
		#yith-quick-view-close,
		.gdpr.gdpr-privacy-bar .gdpr-agreement,
		.gdpr.gdpr-reconsent-bar .gdpr-agreement,
        button:hover,
        .button:hover,
        input[type="button"]:hover,
        input[type="reset"]:hover,
        input[type="submit"]:hover,
        .entry-content .wp-block-button__link:hover,
        .wp-block-button__link:hover,
        .woocommerce #respond input#submit.alt:hover,
        .woocommerce a.button.alt:hover,
        .woocommerce button.button.alt:hover,
        .woocommerce input.button.alt:hover,
        .screen-reader-text:focus,
        .top-bar .main-navigation.toggled,
        button, .button, input[type="button"],
        input[type="reset"], input[type="submit"],
        .entry-content .wp-block-button__link,
        .wp-block-button__link,
        .woocommerce #respond input#submit.alt,
        .woocommerce a.button.alt,
        .woocommerce button.button.alt,
        .woocommerce input.button.alt {
		    background-color: <?php echo $accent_color; ?>;
		}
		input:-webkit-autofill,
		input:-webkit-autofill:hover,
		input:-webkit-autofill:focus
		textarea:-webkit-autofill,
		textarea:-webkit-autofill:hover
		textarea:-webkit-autofill:focus,
		select:-webkit-autofill,
		select:-webkit-autofill:hover,
		select:-webkit-autofill:focus {
		    -webkit-text-fill-color: <?php echo $accent_color; ?>;
		}

		#colophon .footer-made-w-love {
		    color: <?php echo $accent_color; ?>;
		    border-right: 3px solid <?php echo $accent_color; ?>;
		    border-left: 3px solid <?php echo $accent_color; ?>;
		}

        .main-navigation .menu-principal-container > ul li > a:hover,
        .main-navigation .menu-principal-container > ul li > a:focus,
        .main-navigation .menu-principal-container > ul li:last-child > a,
        a:hover,
        a:focus,
        a:active,
        input[type="text"]:focus,
        input[type="email"]:focus,
        input[type="url"]:focus,
        input[type="password"]:focus,
        input[type="number"]:focus,
        input[type="tel"]:focus,
        input[type="submit"]:focus,
        input[type="range"]:focus,
        input[type="date"]:focus,
        input[type="month"]:focus,
        input[type="week"]:focus,
        input[type="time"]:focus,
        input[type="datetime"]:focus,
        input[type="datetime-local"]:focus,
        input[type="color"]:focus,
        textarea:focus,
        select:focus,
        .search-form input[type="search"]:focus,
        .search-form input[type="search"]:hover,
        .search-form input[type="submit"]:focus,
        .search-form input[type="submit"]:hover,
        .is-style-outline,
        .category-navigation .category-list li a,
        div#ez-toc-container .ez-toc-btn-default i {
             color: <?php echo $accent_color; ?>;
        }
        .entry-content a,
        .entry-content ul li a,
        .entry-content p a,
        .entry-content table a,
        .search-page a,
        .search-page ul li a,
        .search-page p a,
        .search-page table a {
            color: <?php echo $accent_color; ?>;
            border-bottom: 2px solid <?php echo $accent_color; ?>;
        }
        #form-toggle,
        .gform_wrapper .gform_footer .button,
        .gform_wrapper .gform_page_footer .button,
        .gform_wrapper .gform_footer button,
        .gform_wrapper .gform_page_footer button {
            background-color: <?php echo $accent_color; ?>;
            border: 2px solid <?php echo $accent_color; ?>;
        }
        .category-navigation .category-list li a:hover {
            border-bottom: 2px solid <?php echo $accent_color; ?>;
        }
        #order-toggle,
        button:hover,
        button:focus,
        button:active,
        .button:hover,
        .button:focus,
        .button:active,
        input[type="button"]:hover,
        input[type="button"]:focus,
        input[type="button"]:active,
        input[type="reset"]:hover,
        input[type="reset"]:focus,
        input[type="reset"]:active,
        input[type="submit"]:hover,
        input[type="submit"]:focus,
        input[type="submit"]:active,
        .entry-content .wp-block-button__link:hover,
        .entry-content .wp-block-button__link:focus,
        .entry-content .wp-block-button__link:active,
        .wp-block-button__link:hover,
        .wp-block-button__link:focus,
        .wp-block-button__link:active,
        .woocommerce #respond input#submit.alt:hover,
        .woocommerce #respond input#submit.alt:focus,
        .woocommerce #respond input#submit.alt:active,
        .woocommerce a.button.alt:hover,
        .woocommerce a.button.alt:focus,
        .woocommerce a.button.alt:active,
        .woocommerce button.button.alt:hover,
        .woocommerce button.button.alt:focus,
        .woocommerce button.button.alt:active,
        .woocommerce input.button.alt:hover,
        .woocommerce input.button.alt:focus,
        .woocommerce input.button.alt:active,
        .menu-toggle:hover,
        .menu-toggle:active,
        .menu-toggle:focus {
            border: 2px solid <?php echo $accent_color; ?>;
            color: <?php echo $accent_color; ?>;
        }
    </style>

	<?php
}
add_action( 'wp_head', 'wglop_comercio_theme_get_customizer_css');

function wglop_comercio_theme_scripts() {

	wp_enqueue_style( 'wglop-comercio-theme-style', get_stylesheet_uri() );

	wp_deregister_script( 'wglop-comercio-theme-jquery');
	wp_enqueue_script( 'wglop-comercio-theme-jquery', get_template_directory_uri() . '/js/jquery-min.js', array(), null, true);
	wp_enqueue_script( 'wglop-comercio-theme-navigation', get_template_directory_uri() . '/js/navigation-min.js', array(), '1', true );
	wp_enqueue_script( 'wglop-comercio-theme-customize', get_template_directory_uri() . '/js/customize-min.js', array(), '1', true );
	wp_enqueue_script( 'wglop-comercio-theme-assets-event-move', get_template_directory_uri() . '/js/jquery.event.move-min.js', array(), '1', true );
	wp_enqueue_script( 'wglop-comercio-theme-assets-twentytwenty', get_template_directory_uri() . '/js/jquery.twentytwenty-min.js', array(), '1', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wglop_comercio_theme_scripts', 99 );

add_theme_support( 'align-wide' );

add_theme_support( 'wp-block-styles' );

add_theme_support( 'responsive-embeds' );

add_theme_support('editor-styles');

add_editor_style( 'style-editor.css' );

// Widgets areas support
function wglop_comercio_theme_widgets_init() {

	register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'wglop-comercio-theme' ),
		'id'            => 'footer-widget',
		'description'   => esc_html__( 'Add widget here.', 'wglop-comercio-theme' ),
		'before_widget' => '<article id="widget-footer-%1$s" class="footer-widget">',
		'after_widget'  => '</article>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Login', 'wglop-comercio-theme' ),
		'id'            => 'login-widget',
		'description'   => esc_html__( 'Add widget here.', 'wglop-comercio-theme' ),
		'before_widget' => '<article id="widget-login-%1$s" class="login-widget">',
		'after_widget'  => '</article>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Order', 'wglop-comercio-theme' ),
		'id'            => 'order-widget',
		'description'   => esc_html__( 'Add widget here.', 'wglop-comercio-theme' ),
		'before_widget' => '<article id="widget-order-%1$s" class="order-widget">',
		'after_widget'  => '</article>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Info', 'wglop-comercio-theme' ),
		'id'            => 'info-widget',
		'description'   => esc_html__( 'Add widget here.', 'wglop-comercio-theme' ),
		'before_widget' => '<article id="widget-info-%1$s" class="info-widget">',
		'after_widget'  => '</article>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Home', 'wglop-comercio-theme' ),
		'id'            => 'home-widget',
		'description'   => esc_html__( 'Add widget here.', 'wglop-comercio-theme' ),
		'before_widget' => '<article id="widget-home-%1$s" class="home-widget">',
		'after_widget'  => '</article>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'wglop_comercio_theme_widgets_init' );

// Remove scripts from head
function move_scripts_from_head_to_footer() {
	remove_action( 'wp_head', 'wp_print_scripts' );
	remove_action( 'wp_head', 'wp_print_head_scripts', 9 );
	remove_action( 'wp_head', 'wp_enqueue_scripts', 1 );

	add_action( 'wp_footer', 'wp_print_scripts', 5);
	add_action( 'wp_footer', 'wp_enqueue_scripts', 5);
	add_action( 'wp_footer', 'wp_print_head_scripts', 5);
}
add_action('wp_enqueue_scripts', 'move_scripts_from_head_to_footer');

function force_jquery_to_footer() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), false, NULL, true );
	wp_enqueue_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'force_jquery_to_footer' );

// Remove jQuery from old wp_print_scripts
function remove_jquery_from_wp_print_scripts() {
	wp_deregister_script( 'jquery' );
}
add_action( 'wp_print_scripts', 'remove_jquery_from_wp_print_scripts' );

//Own functions
function wglop_comercio_body_begin() {
	do_action('wglop_comercio_body_begin');
}

// WooCommerce functions
function wglop_comercio_woocommerce_remove_actions() {
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
	remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
	//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
}
add_action('init', 'wglop_comercio_woocommerce_remove_actions');

function wglop_comercio_woocommerce_remove_single_product_image_html( $html, $post_id ) {
	return preg_replace( "!<(a|/a).*?>!", '', $html );
}
add_filter( 'woocommerce_single_product_image_thumbnail_html', 'wglop_comercio_woocommerce_remove_single_product_image_html', 10, 2 );

function wglop_comercio_show_excerpt_shop_page() {
	global $product;
	echo '<section class="woocommerce-loop-product__meta">';
	echo '<p class="woocommerce-loop-product__title">' . $product->post->post_title . '</p>';
	echo '<p class="woocommerce-loop-product__description">' . $product->post->post_excerpt . '</p>';
	echo '<p class="woocommerce-loop-product__price">' . $product->get_price_html() . '</p>';
	echo '</section>';
}
add_action( 'woocommerce_after_shop_loop_item', 'wglop_comercio_show_excerpt_shop_page', 7 );

function wglop_comercio_custom_shop_page_redirect() {
	if( is_shop() ){
		wp_redirect( home_url( '/' ) );
		exit();
	}
}
add_action( 'template_redirect', 'wglop_comercio_custom_shop_page_redirect' );


