<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<?php wglop_comercio_body_begin(); ?>

<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
<header id="masthead" class="site-header" style="
        background-image:
        linear-gradient(rgba(51, 51, 51, 0.4), rgba(26, 26, 26, 0.1)),
        url('<?php echo $thumb['0'];?>');
        ">

    <section class="top-bar">

        <article class="wrapper">

			<?php
			if ( ! is_user_logged_in() ) : ?>
                <nav id="site-login">
                    <div id="form-toggle"><?php _e( 'Sign in', 'wglop-comercio-theme' )?></div>
                    <div id="login-form">
						<?php
						if ( is_active_sidebar( 'login-widget' ) ) : ?>
                            <section class="login-widget-area">
								<?php dynamic_sidebar( 'login-widget' ); ?>
                            </section>
						<?php
						endif; ?>
                    </div>
                </nav>
			<?php
			endif; ?>

			<?php
			if ( has_nav_menu( 'menu-1' ) ) : ?>
                <nav id="site-navigation" class="main-navigation">
                    <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false" aria-label="Menu"></button>
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
					?>
                </nav>
			<?php
			endif; ?>

            <section class="site-title">
				<?php
				$custom_logo_id = get_theme_mod( 'custom_logo' );
				$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				$url = home_url( '/', 'https' );
				if ( has_custom_logo() ) {
					echo '<a href="'. esc_url( $url ) .'" title="logo inicio"><img src="'. esc_url( $logo[0] ) .'" alt="logo ' . get_bloginfo( 'name' ) . '" title="logo ' . get_bloginfo( 'name' ) . '"></a>';
				} else {
					echo '<a href="'. esc_url( $url ) .'" title="logo inicio">'. get_bloginfo( 'name' ) .'</a>';
				}
				?>
            </section>

            <nav id="site-order">
                <div id="order-toggle"><?php _e( 'Cart', 'wglop-comercio-theme' )?></div>
                <div id="order-list">
					<?php
					if ( is_active_sidebar( 'order-widget' ) ) : ?>
						<?php dynamic_sidebar( 'order-widget' );
					else : ?>
                        <h3><?php esc_html_e( 'This is the Order Widget', 'wglop-comercio-theme' );?></h3>
                        <p><?php esc_html_e( 'You need to add the WooCommerce cart widget inside Order Widget Area in WordPress Dashboard > Appearence > Widgets', 'wglop-comercio-theme' );?></p>
					<?php
					endif; ?>
                </div>
            </nav>

        </article>

    </section><!-- .top-bar -->

	<?php
	if  ( is_front_page() ) : ?>
        <section class="hero">
			<?php
			if ( is_active_sidebar( 'home-widget' ) ) : ?>
				<?php dynamic_sidebar( 'home-widget' );
			else : ?>
                <h1><?php esc_html_e( 'This is the Front Page', 'wglop-comercio-theme' );?></h1>
                <p><?php esc_html_e( 'You need to add any widget in Home Widget Area in WordPress Dashboard > Appearence > Widgets', 'wglop-comercio-theme' );?></p>
			<?php
			endif; ?>
        </section>
	<?php
    elseif ( is_404() ) : ?>
        <section class="hero">
            <h1><?php esc_html_e( 'Error 404', 'wglop-comercio-theme' ) ?></h1>
        </section>
	<?php
    elseif ( is_search() ) : ?>
        <section class="hero">
            <h1><?php esc_html_e( 'Search results for:', 'wglop-comercio-theme' );?> <?php echo get_search_query(); ?></h1>
        </section>
	<?php
	else : ?>
        <section class="hero">
            <h1><?php the_title(); ?></h1>
        </section>
	<?php
	endif; ?>

</header><!-- #masthead -->