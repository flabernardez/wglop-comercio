// DOM.event.move
//
// 2.0.0
//
// Stephen Band
//
// Triggers 'movestart', 'move' and 'moveend' events after
// mousemoves following a mousedown cross a distance threshold,
// similar to the native 'dragstart', 'drag' and 'dragend' events.
// Move events are throttled to animation frames. Move event objects
// have the properties:
//
// pageX:
// pageY:     Page coordinates of pointer.
// startX:
// startY:    Page coordinates of pointer at movestart.
// distX:
// distY:     Distance the pointer has moved since movestart.
// deltaX:
// deltaY:    Distance the finger has moved since last event.
// velocityX:
// velocityY: Average velocity over last few events.
!function(e){"function"==typeof define&&define.amd?define([],e):"undefined"!=typeof module&&null!==module&&module.exports?module.exports=e:e()}(function(){function i(e){return new CustomEvent(e,P)}function m(e){return e[B]||(e[B]={})}function a(e,t,n,o,i){function a(e){n(e,o)}t=t.split(O);for(var u=m(e),c=t.length,r,d;c--;)(r=u[d=t[c]]||(u[d]=[])).push([n,a]),e.addEventListener(d,a)}function u(e,t,n,o){t=t.split(O);var i=m(e),a=t.length,u,c,r;if(i)for(;a--;)if(c=i[u=t[a]])for(r=c.length;r--;)c[r][0]===n&&(e.removeEventListener(u,c[r][1]),c.splice(r,1))}function d(e,t,n){
// Don't cache events. It prevents you from triggering an event of a
// given type from inside the handler of another event of that type.
var o=i(t);n&&x(o,n),e.dispatchEvent(o)}
// Constructors
function c(e){function t(e){o?(n(),R(t),o=!(i=!0)):i=!1}var n=e,o=!1,i=!1;this.kick=function(e){o=!0,i||t()},this.end=function(e){var t=n;e&&(
// If the timer is not running, simply call the end callback.
i?(n=o?function(){t(),e()}:e,o=!0):e())}}
// Functions
function f(){}function r(e){e.preventDefault()}function t(e){return!!D[e.target.tagName.toLowerCase()]}function n(e){
// Ignore mousedowns on any button other than the left (or primary)
// mouse button, or when a modifier key is pressed.
return 1===e.which&&!e.ctrlKey&&!e.altKey}function v(e,t){var n,o;if(e.identifiedTouch)return e.identifiedTouch(t);
// touchList.identifiedTouch() does not exist in
// webkit yet… we must do the search ourselves...
for(n=-1,o=e.length;++n<o;)if(e[n].identifier===t)return e[n]}function s(e,t){var n=v(e.changedTouches,t.identifier);
// This isn't the touch you're looking for.
if(n&&(n.pageX!==t.pageX||n.pageY!==t.pageY))return n;
// Chrome Android (at least) includes touches that have not
// changed in e.changedTouches. That's a bit annoying. Check
// that this touch has changed.
}
// Handlers that decide when the first movestart is triggered
function e(e){
// Ignore non-primary buttons
n(e)&&(
// Ignore form and interactive elements
t(e)||(a(document,N.move,o,e),a(document,N.cancel,l,e)))}function o(e,t){y(e,t,e,p)}function l(e,t){p()}function p(){u(document,N.move,o),u(document,N.cancel,l)}function g(e){
// Don't get in the way of interaction with form elements
if(!D[e.target.tagName.toLowerCase()]){var t=e.changedTouches[0],n={target:t.target,pageX:t.pageX,pageY:t.pageY,identifier:t.identifier,
// The only way to make handlers individually unbindable is by
// making them unique.
touchmove:function(e,t){h(e,t)},touchend:function(e,t){X(e,t)}};
// iOS live updates the touch objects whereas Android gives us copies.
// That means we can't trust the touchstart object to stay the same,
// so we must copy the data. This object acts as a template for
// movestart, move and moveend event objects.
a(document,z.move,n.touchmove,n),a(document,z.cancel,n.touchend,n)}}function h(e,t){var n=s(e,t);n&&y(e,t,n,Y)}function X(e,t){var n;v(e.changedTouches,t.identifier)&&Y(t)}function Y(e){u(document,z.move,e.touchmove),u(document,z.cancel,e.touchend)}function y(e,t,n,o){var i=n.pageX-t.pageX,a=n.pageY-t.pageY;
// Do nothing if the threshold has not been crossed.
i*i+a*a<L*L||w(e,t,n,i,a,o)}function w(e,t,n,o,i,a){var u=e.targetTouches,c=e.timeStamp-t.timeStamp,r={altKey:e.altKey,ctrlKey:e.ctrlKey,shiftKey:e.shiftKey,startX:t.pageX,startY:t.pageY,distX:o,distY:i,deltaX:o,deltaY:i,pageX:n.pageX,pageY:n.pageY,velocityX:o/c,velocityY:i/c,identifier:t.identifier,targetTouches:u,finger:u?u.length:1,enableMove:function(){this.moveEnabled=!0,this.enableMove=f,e.preventDefault()}};
// Trigger the movestart event.
d(t.target,"movestart",r),
// Unbind handlers that tracked the touch or mouse up till now.
a(t)}
// Handlers that control what happens following a movestart
function b(e,t){var n=t.timer;t.touch=e,t.timeStamp=e.timeStamp,n.kick()}function T(e,t){var n=t.target,o=t.event,i=t.timer;E(),C(n,o,i,function(){
// Unbind the click suppressor, waiting until after mouseup
// has been handled.
setTimeout(function(){u(n,"click",r)},0)})}function E(){u(document,N.move,b),u(document,N.end,T)}function S(e,t){var n=t.event,o=t.timer,i=s(e,n);i&&(
// Stop the interface from gesturing
e.preventDefault(),n.targetTouches=e.targetTouches,t.touch=i,t.timeStamp=e.timeStamp,o.kick())}function k(e,t){var n=t.target,o=t.event,i=t.timer,a;
// This isn't the touch you're looking for.
v(e.changedTouches,o.identifier)&&(K(t),C(n,o,i))}function K(e){u(document,z.move,e.activeTouchmove),u(document,z.end,e.activeTouchend)}
// Logic for triggering move and moveend events
function j(e,t,n){var o=n-e.timeStamp;e.distX=t.pageX-e.startX,e.distY=t.pageY-e.startY,e.deltaX=t.pageX-e.pageX,e.deltaY=t.pageY-e.pageY,
// Average the velocity of the last few events using a decay
// curve to even out spurious jumps in values.
e.velocityX=.3*e.velocityX+.7*e.deltaX/o,e.velocityY=.3*e.velocityY+.7*e.deltaY/o,e.pageX=t.pageX,e.pageY=t.pageY}function C(e,t,n,o){n.end(function(){return d(e,"moveend",t),o&&o()})}
// Set up the DOM
function Q(e){function t(e){j(n,o.touch,o.timeStamp),d(o.target,"move",n)}if(!e.defaultPrevented&&e.moveEnabled){var n={startX:e.startX,startY:e.startY,pageX:e.pageX,pageY:e.pageY,distX:e.distX,distY:e.distY,deltaX:e.deltaX,deltaY:e.deltaY,velocityX:e.velocityX,velocityY:e.velocityY,identifier:e.identifier,targetTouches:e.targetTouches,finger:e.finger},o={target:e.target,event:n,timer:new c(t),touch:void 0,timeStamp:e.timeStamp};void 0===e.identifier?(
// We're dealing with a mouse event.
// Stop clicks from propagating during a move
a(e.target,"click",r),a(document,N.move,b,o),a(document,N.end,T,o)):(
// In order to unbind correct handlers they have to be unique
o.activeTouchmove=function(e,t){S(e,t)},o.activeTouchend=function(e,t){k(e,t)},
// We're dealing with a touch.
a(document,z.move,o.activeTouchmove,o),a(document,z.end,o.activeTouchend,o))}}function q(e){e.enableMove()}function A(e){e.enableMove()}function F(e){e.enableMove()}function M(e){var o=e.handler;e.handler=function(e){for(
// Copy move properties across from originalEvent
var t=G.length,n;t--;)e[n=G[t]]=e.originalEvent[n];o.apply(this,arguments)}}var x=Object.assign||window.jQuery&&jQuery.extend,L=8,R=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(e,t){return window.setTimeout(function(){e()},25)};
// Number of pixels a pressed pointer travels before movestart
// event is fired.
// Shim for customEvent
// see https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent#Polyfill
!function(){function e(e,t){t=t||{bubbles:!1,cancelable:!1,detail:void 0};var n=document.createEvent("CustomEvent");return n.initCustomEvent(e,t.bubbles,t.cancelable,t.detail),n}if("function"==typeof window.CustomEvent)return;e.prototype=window.Event.prototype,window.CustomEvent=e}();var D={textarea:!0,input:!0,select:!0,button:!0},N={move:"mousemove",cancel:"mouseup dragstart",end:"mouseup"},z={move:"touchmove",cancel:"touchend",end:"touchend"},O=/\s+/,P={bubbles:!0,cancelable:!0},B="function"==typeof Symbol?Symbol("events"):{};
// jQuery special events
//
// jQuery event objects are copies of DOM event objects. They need
// a little help copying the move properties across.
if(a(document,"mousedown",e),a(document,"touchstart",g),a(document,"movestart",Q),window.jQuery){var G="startX startY pageX pageY distX distY deltaX deltaY velocityX velocityY".split(" ");jQuery.event.special.movestart={setup:function(){
// Do listen to DOM events
// Movestart must be enabled to allow other move events
return a(this,"movestart",q),!1},teardown:function(){return u(this,"movestart",q),!1},add:M},jQuery.event.special.move={setup:function(){return a(this,"movestart",A),!1},teardown:function(){return u(this,"movestart",A),!1},add:M},jQuery.event.special.moveend={setup:function(){return a(this,"movestart",F),!1},teardown:function(){return u(this,"movestart",F),!1},add:M}}});