<?php

get_header(); ?>

    <main id="content" class="site-content">

	    <?php
	    if ( have_posts() ) :

		    /* Start the Loop */
		    while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <section class="wrapper">


                        <section class="entry-content">

                            <?php
                            if ( is_page() && !is_front_page() ) {
                                if ( function_exists('yoast_breadcrumb') ) {
                                    yoast_breadcrumb( '<section id="breadcrumbs">','</section>' );
                                }
                            };

                            the_content( sprintf(
                                wp_kses(
                                /* translators: %s: Name of current post. Only visible to screen readers */
                                    __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'wglop-comercio-theme' ),
                                    array(
                                        'span' => array(
                                            'class' => array(),
                                        ),
                                    )
                                ),
                                get_the_title()
                            ) );

                            wp_link_pages( array(
                                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wglop-comercio-theme' ),
                                'after'  => '</div>',
                            ) );
                            ?>

                        </section><!-- .entry-content -->

                        <?php
                        if( function_exists("kk_star_ratings")) : ?>

                        <section class="valoracion">
                            <h4>Califica este contenido</h4>
                            <?php kk_star_ratings($pid); ?>
                        </section>

                        <?php
                        endif;

                        if ( ! is_front_page() ) {
                            if ( comments_open() || get_comments_number() ) :

                                comments_template();

                            endif;
                        }
                        ?>

                    </section>

                </article><!-- #post-<?php the_ID(); ?> -->

		    <?php
            endwhile;

		    the_posts_navigation();

	    else :

		    get_template_part( 'template-parts/content', 'none' );

	    endif;

	    ?>

    </main><!-- #content -->

<?php

get_footer(); ?>